# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 18:48:56 2020

@author: Frédéric Auchère
"""

import glob
from eui.processing import rectify

from astropy import visualization
from astropy.io import fits
from eui.fsi import display
import matplotlib.pyplot as plt
import sunpy.visualization.colormaps as cm
from eui import euiprep
import numpy as np

path = 'C:\\archive\\EUI\\data\\L1\\2020\\05\\30\\'
    
files = glob.glob(path + 'solo_L1_eui-hrieuv174*.fits')

cmap = plt.get_cmap('sdoaia171')

stretch = visualization.LinearStretch()

opt_calibration = {"gitlab_local": False,
                   "orient_image":False,
                   "darkframe_correction":"local",
                   "flatfield_correction":"local",
                   "dir_results":"C:\\archive\\EUI\\data\\L1\\2020\\05\\30\\L3\\",
                   "limbfit":False,
                   "geometric_rectifications":False,
                   "normalize_it":True,
                   "save_L2":False}

shape = (2400, 2400)
lonlims = (249.2, 288.2)
latlims = (-11.5, 27.5)

solar_r = 1.005

for i, f in enumerate(files):

    euimap = euiprep.euiprep(f, **opt_calibration)

    lon = euimap.header['CRLN_OBS']
    lat = euimap.header['CRLT_OBS']
    ry = solar_r*np.degrees(np.arctan(6.96e8/euimap.header['DSUN_OBS']))*3600/euimap.header['CDELT1']
    rx = solar_r*np.degrees(np.arctan(6.96e8/euimap.header['DSUN_OBS']))*3600/euimap.header['CDELT2']
    roll = euimap.header['CROTA'] - 0.35
    xc = -(euimap.header['CRVAL1'] - 120.0)/euimap.header['CDELT1']
    yc = -(euimap.header['CRVAL2'] + 125.0)/euimap.header['CDELT2']
    xc += euimap.header['CRPIX1'] - 1 
    yc += euimap.header['CRPIX2'] - 1 

    spherical = rectify.SphericalTransform(xc, yc, rx, ry, lon, lat, roll)
    spherizer = rectify.Rectifier(spherical)
    carrington = spherizer(euimap._data, shape, lonlims, latlims, opencv=True, order=2)
    
    out = f.replace('_L1_', '_L3_')
    out = out.replace('.fits', '_carrington.fits')

    header = fits.Header()
    header['INSTRUME'] = euimap.header['INSTRUME']
    header['DATE-OBS'] = euimap.header['DATE-OBS']
    header['WAVELNTH'] = euimap.header['WAVELNTH']
    header['TELESCOP'] = euimap.header['TELESCOP']
    header['DSUN_OBS'] = euimap.header['DSUN_OBS']
    header['CRPIX1'] = (shape[0] + 1)/2 + 1
    header['CRPIX2'] = (shape[1] + 1)/2 + 1
    header['CRVAL1'] = (lonlims[1] + lonlims[0])/2
    header['CRVAL2'] = (latlims[1] + latlims[0])/2
    header['CDELT1'] = (lonlims[1] - lonlims[0])/shape[0]
    header['CDELT2'] = (latlims[1] - latlims[0])/shape[1]
    fits.writeto(out, carrington, header=header, overwrite=True)
     
    out = f.replace('_L1_', '_L3_')
    out = out.replace('.fits', '_carrington.png')
    
    if i == 0:
        interval = visualization.AsymmetricPercentileInterval(0.1, 99.95)
        vmin, vmax = interval.get_limits(euimap._data)

    interval = visualization.ManualInterval(vmin, vmax)
    transform = stretch + interval
    disp = transform(carrington)
   
    # display.savepng(str(out), disp, cmap=cmap)


# AIA

path = 'C:\\archive\\aia\\'

waves = ['94', '131', '171', '193', '211', '304', '335']
solar_r = [1.005, 1.005, 1.005, 1.005, 1.005, 1.01, 1.005]

for w, sr in zip(waves, solar_r):

    files = glob.glob(path + 'aia.lev1.' + w + '.*image_lev1.fits')
    
    for i, f in enumerate(files):
    
        with fits.open(f) as hdu:
            hdu[1].verify('fix')
            aiamap = hdu[1].data
            aiaheader = hdu[1].header
        
        lon = aiaheader['CRLN_OBS']
        lat = aiaheader['CRLT_OBS']
        ry = sr*np.degrees(np.arctan(6.96e8/aiaheader['DSUN_OBS']))*3600/aiaheader['CDELT1']
        rx = sr*np.degrees(np.arctan(6.96e8/aiaheader['DSUN_OBS']))*3600/aiaheader['CDELT2']
        roll = aiaheader['CROTA2']
        xc = aiaheader['CRPIX1'] - 1 
        yc = aiaheader['CRPIX2'] - 1 
    
        spherical = rectify.SphericalTransform(xc, yc, rx, ry, lon, lat, roll)
        spherizer = rectify.Rectifier(spherical)
        carrington = spherizer(aiamap, shape, lonlims, latlims, opencv=True, order=2)
    
        out = f.replace('C:\\archive\\aia\\', 'C:\\archive\\aia\\' + w + '\\')
        out = out.replace('lev1', 'lev3')
        out = out.replace('.fits', '_carrington.fits')
    
        header = fits.Header()
        header['INSTRUME'] = aiaheader['INSTRUME']
        header['DATE-OBS'] = aiaheader['DATE-OBS']
        header['WAVELNTH'] = aiaheader['WAVELNTH']
        header['TELESCOP'] = aiaheader['TELESCOP']
        header['DSUN_OBS'] = aiaheader['DSUN_OBS']
        header['CRPIX1'] = (shape[0] + 1)/2
        header['CRPIX2'] = (shape[1] + 1)/2
        header['CRVAL1'] = (lonlims[1] + lonlims[0])/2
        header['CRVAL2'] = (latlims[1] + latlims[0])/2
        header['CDELT1'] = (lonlims[1] - lonlims[0])/shape[0]
        header['CDELT2'] = (latlims[1] - latlims[0])/shape[1]
        fits.writeto(out, carrington, header=header, overwrite=True)
    
        out = f.replace('C:\\archive\\aia\\', 'C:\\archive\\aia\\' + w + '\\')
        out = out.replace('lev1', 'lev3')
        out = out.replace('.fits', '_carrington.png')
        print(out)
        
        if i == 0:
            interval = visualization.AsymmetricPercentileInterval(45, 95)
            vmin, vmax = interval.get_limits(aiamap)
    
        interval = visualization.ManualInterval(vmin, vmax)
        transform = stretch + interval
        disp = transform(carrington)
       
        # display.savepng(str(out), disp, cmap=cmap)

